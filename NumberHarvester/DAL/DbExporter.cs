﻿using System.Collections.Generic;
using System.Linq;
using FileContentModel = NumberHarvester.Model.FileContent;

namespace NumberHarvester.DAL
{
    static class DbExporter
    {
        /// <summary>Экспорт в БД.</summary>
        /// <param name="content">Содержимое файлов.</param>
        public static void Export(IEnumerable<FileContentModel> content)
        {
            using (var ctx = new FilesContentContext())
            {
                var dbContent = content.Select(f => new FileContent { FileName = f.FileName, Numbers = f.Numbers });
                ctx.FilesContet.AddRange(dbContent);
                ctx.SaveChanges();
            }
        }
    }
}