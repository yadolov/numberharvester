﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NumberHarvester.DAL
{
    /// <summary>
    /// Содержимое файла (Entity).
    /// </summary>
    public class FileContent
    {
        /// <summary>Уникальный идентификатор файла.</summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        /// <summary>Наименование файла.</summary>
        public string FileName { get; set; }
        /// <summary>Строка с числами.</summary>
        public string Numbers { get; set; }
    }
}