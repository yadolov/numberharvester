﻿using System.Data.Entity;

namespace NumberHarvester.DAL
{
    /// <summary>
    /// Контекст БД.
    /// </summary>
    class FilesContentContext : DbContext
    {
        /// <summary>Содержимое файлов.</summary>
        public DbSet<FileContent> FilesContet { get; set; }
    }
}