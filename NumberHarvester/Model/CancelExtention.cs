﻿using System.Collections.Generic;
using System.Threading;

namespace NumberHarvester.Model
{
    static class CancelExtention
    {
        /// <summary>
        /// Расширение для отмены перечисления.
        /// </summary>
        /// <typeparam name="T">Тип элемента перечисления</typeparam>
        /// <param name="en">Перечисление</param>
        /// <param name="token">Токен отмены</param>
        /// <returns>Перечисление</returns>
        public static IEnumerable<T> WithCancellation<T>(this IEnumerable<T> en, CancellationToken token)
        {
            foreach (var item in en)
            {
                token.ThrowIfCancellationRequested();
                yield return item;
            }
        }
    }
}