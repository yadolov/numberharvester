﻿using System;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace NumberHarvester.Model
{
    /// <summary>
    /// Модель приложения.
    /// </summary>
    public class MainModel
    {
        /// <summary>Маска поиска файлов.</summary>
        private const string SearchMask = "*.txt";
        /// <summary>Разделитель чисел в строке.</summary>
        private const string NumberSeparator = ", ";
        /// <summary>Разделители в файле.</summary>
        private static readonly char[] Separators;
        /// <summary>Количество фоновых потоков.</summary>
        private static readonly int WorkerThreadCount;
        /// <summary>Флаг поиска только в директории верхнего уровня (без дочерних).</summary>
        private static readonly bool TopDirectoryOnly;
        /// <summary>Планировщик выполнения задач UI-потока.</summary>
        private readonly TaskScheduler _uiScheduler;
        /// <summary>Содержимое найденных файлов.</summary>
        private readonly ObservableCollection<FileContent> _filesContent;
        /// <summary>Отмена текущей фоновой активности.</summary>
        private CancellationTokenSource _cancellationTokenSource;
        /// <summary>Состояние модели приложения.</summary>
        private ModelState _modelState;

        /// <summary>Статический конструктор.</summary>
        static MainModel()
        {
            Separators = new[]
                                {
                                    ' ', // пробел
                                    '\t', // табуляция
                                    '`', '\'', // апостроф
                                    '[', ']', '(', ')', '{', '}', '<', '>', // скобки
                                    ':', // двоеточие
                                    ',', //запятая
                                    '!', // восклицательный знак
                                    '.', // точка
                                    '-', // дефис
                                    '?', // вопросительный знак
                                    '‘', '’', '“', '”', '«', '»', // кавычки
                                    ';', // точка с запятой
                                    '/' // косая черта
                                };

            // Вычитываем конфигурационные данные
            var workerThreadCountText = ConfigurationManager.AppSettings["workerThreadCount"];
            int workerThreadCount;
            WorkerThreadCount = int.TryParse(workerThreadCountText, out workerThreadCount) ? workerThreadCount : Environment.ProcessorCount;
            var topDirectoryOnlyText = ConfigurationManager.AppSettings["topDirectoryOnly"];
            bool topDirectoryOnly;
            TopDirectoryOnly = !bool.TryParse(topDirectoryOnlyText, out topDirectoryOnly) || topDirectoryOnly;
        }

        /// <summary>Конструктор.</summary>
        public MainModel()
        {
            _filesContent = new ObservableCollection<FileContent>();
            _uiScheduler = TaskScheduler.FromCurrentSynchronizationContext();
        }

        /// <summary>Содержимое найденных файлов.</summary>
        public ObservableCollection<FileContent> FilesContent
        {
            get { return _filesContent; }
        }

        /// <summary>Состояние модели приложения.</summary>
        public ModelState ModelState
        {
            get { return _modelState; }
            private set
            {
                if (_modelState == value)
                    return;
                _modelState = value;
                RaiseSearchStateChanged();
            }
        }

        /// <summary>Событие смены состояния модели приложения.</summary>
        public event EventHandler ModelStateChanged;

        /// <summary>Запуск поиска.</summary>
        /// <param name="folderPath">Путь к директории поиска</param>
        public void StartSearch(string folderPath)
        {
            ModelState = ModelState.Searching;
            _cancellationTokenSource = new CancellationTokenSource();
            _filesContent.Clear();

            Task.Factory.StartNew(() =>
            {
                try
                {
                    var files = Directory
                        .EnumerateFiles(folderPath, SearchMask,
                            TopDirectoryOnly ? SearchOption.TopDirectoryOnly : SearchOption.AllDirectories);
                    Parallel.ForEach(files,
                        new ParallelOptions
                        {
                            MaxDegreeOfParallelism = WorkerThreadCount,
                            CancellationToken = _cancellationTokenSource.Token
                        },
                        filePath =>
                        {
                            var fileContent = new FileContent
                            {
                                FileName = Path.GetFileName(filePath),
                                Processing = true,
                            };

                            Task.Factory.StartNew(
                                () => _filesContent.Add(fileContent),
                                _cancellationTokenSource.Token,
                                TaskCreationOptions.None,
                                _uiScheduler);

                            var numbers = ReadNumbersFromFile(filePath);

                            Task.Factory.StartNew(
                                () =>
                                {
                                    fileContent.Numbers = numbers;
                                    fileContent.Processing = false;
                                },
                                _cancellationTokenSource.Token,
                                TaskCreationOptions.None,
                                _uiScheduler);
                        });
                }
                catch (OperationCanceledException)
                {
                }
                finally
                {
                    Task.Factory.StartNew(
                        () => ModelState = ModelState.Idle,
                        CancellationToken.None,
                        TaskCreationOptions.None,
                        _uiScheduler);
                }
            });
        }

        /// <summary>Останов поиска.</summary>
        public void StopSearch()
        {
            if (_cancellationTokenSource != null)
                _cancellationTokenSource.Cancel();
        }

        /// <summary>Экспорт в БД.</summary>
        public void ExportToDb()
        {
            ModelState = ModelState.Exporting;

            Task.Factory.StartNew(() =>
            {
                try
                {
                    DAL.DbExporter.Export(FilesContent.Where(f => !f.Processing));
                }
                finally 
                {
                    Task.Factory.StartNew(
                        () => ModelState = ModelState.Idle,
                        CancellationToken.None,
                        TaskCreationOptions.None,
                        _uiScheduler);
                }
            });
        }

        /// <summary>Вычитывание чисел в виде строки из файла.</summary>
        /// <param name="filePath">Путь к файлу.</param>
        /// <returns>Строка с числами</returns>
        private string ReadNumbersFromFile(string filePath)
        {
            return string.Join(NumberSeparator,
                File.ReadLines(filePath, Encoding.ASCII)
                    .WithCancellation(_cancellationTokenSource.Token)
                    .SelectMany(
                        line => line.Split(Separators).Where(s => s.Length > 0 && s.All(Char.IsDigit))));
        }

        /// <summary>Генерация события изменения состояния модели приложения.</summary>
        private void RaiseSearchStateChanged()
        {
            var eh = ModelStateChanged;
            if (eh != null)
                eh(this, EventArgs.Empty);
        }
    }
}