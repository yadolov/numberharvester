﻿using System;
using GalaSoft.MvvmLight;

namespace NumberHarvester.Model
{
    /// <summary>
    /// Содержимое файла.
    /// </summary>
    public class FileContent : ObservableObject
    {
        private string _fileName;
        private bool _processing;
        private string _numbers;

        /// <summary>Наименование файла.</summary>
        public string FileName
        {
            get { return _fileName; }
            set
            {
                if (String.CompareOrdinal(_fileName, value) == 0)
                    return;
                _fileName = value;
                RaisePropertyChanged(() => FileName);
            }
        }

        /// <summary>Признак обработки файла.</summary>
        public bool Processing
        {
            get { return _processing; }
            set
            {
                if (_processing == value)
                    return;
                _processing = value;
                RaisePropertyChanged(() => Processing);
            }
        }

        /// <summary>Строка с числами.</summary>
        public string Numbers
        {
            get { return _numbers; }
            set
            {
                if (String.CompareOrdinal(_numbers, value) == 0)
                    return;
                _numbers = value;
                RaisePropertyChanged(() => Numbers);
            }
        }
    }
}