﻿namespace NumberHarvester.Model
{
    /// <summary>
    /// Состояние модели приложения.
    /// </summary>
    public enum ModelState
    {
        /// <summary>
        /// В простое.
        /// </summary>
        Idle = 0,
        /// <summary>
        /// Поиск.
        /// </summary>
        Searching,
        /// <summary>
        /// Экспорт в БД.
        /// </summary>
        Exporting
    }
}