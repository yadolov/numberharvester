﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using NumberHarvester.Model;

namespace NumberHarvester.ViewModel
{
    /// <summary>
    /// Модель представления приложения.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>Модель приложения.</summary>
        private readonly MainModel _model;
        /// <summary>Путь к директории поиска.</summary>
        private string _folderPath;

        /// <summary>Конструктор.</summary>
        public MainViewModel()
        {
            _model = new MainModel();
            _model.ModelStateChanged += OnModelStateChanged;

            SelectFolderCommand = new RelayCommand(SelectFolder, () => ModelState == ModelState.Idle);
            ExportToDbCommand = new RelayCommand(ExportToDb, () => ModelState == ModelState.Idle && FilesContent.Any());
            StartSearchCommand = new RelayCommand(StartSearch, () => ModelState == ModelState.Idle && Directory.Exists(FolderPath));
            StopSearchCommand = new RelayCommand(StopSearch);
            WindowClosedCommand = new RelayCommand(() => _model.StopSearch());
        }

        /// <summary>Команда выбора директории.</summary>
        public ICommand SelectFolderCommand { get; private set; }
        /// <summary>Команда экспорта в БД.</summary>
        public ICommand ExportToDbCommand { get; private set; }
        /// <summary>Команда запуска поиска.</summary>
        public ICommand StartSearchCommand { get; private set; }
        /// <summary>Команда останова поиска.</summary>
        public ICommand StopSearchCommand { get; private set; }
        /// <summary>Команда закрытия окна приложения.</summary>
        public ICommand WindowClosedCommand { get; private set; }

        /// <summary>Директория поиска.</summary>
        public string FolderPath
        {
            get { return _folderPath; }
            set
            {
                if (String.CompareOrdinal(_folderPath, value) == 0)
                    return;
                _folderPath = value;
                RaisePropertyChanged(() => FolderPath);
            }
        }

        /// <summary>Содержимое найденных файлов.</summary>
        public ObservableCollection<FileContent> FilesContent
        {
            get { return _model.FilesContent; }
        }

        /// <summary>Состояние модели приложения.</summary>
        public ModelState ModelState
        {
            get { return _model.ModelState; }
        }

        /// <summary>Обработка смены состояния модели приложения.</summary>
        private void OnModelStateChanged(object sender, EventArgs e)
        {
            RaisePropertyChanged(() => ModelState);
            CommandManager.InvalidateRequerySuggested();
        }

        /// <summary>Обработка команды выбора директории поиска.</summary>
        private void SelectFolder()
        {
            var dlg = new FolderBrowserDialog();
            if (Directory.Exists(FolderPath))
                dlg.SelectedPath = FolderPath;
            var result = dlg.ShowDialog();
            if (result != DialogResult.OK)
                return;
            FolderPath = dlg.SelectedPath;
        }

        /// <summary>Обработка команды экспорта в БД.</summary>
        private void ExportToDb()
        {
            _model.ExportToDb();
        }

        /// <summary>Обработка команды запуска поиска.</summary>
        private void StartSearch()
        {
            _model.StartSearch(FolderPath);
        }

        /// <summary>Обработка команды останова поиска.</summary>
        private void StopSearch()
        {
            _model.StopSearch();
        }
    }
}